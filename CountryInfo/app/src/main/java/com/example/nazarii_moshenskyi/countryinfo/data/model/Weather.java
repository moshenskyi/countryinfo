package com.example.nazarii_moshenskyi.countryinfo.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "weather",
        foreignKeys = { @ForeignKey(entity = Month.class,
                parentColumns = "id",
                childColumns = "month_id", onDelete = ForeignKey.CASCADE) })
public class Weather {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "month_id")
    private int monthId;

    @Ignore
    private List<Month> months = null;

    public Weather() {
        months = new ArrayList<>();
    }

    public List<Month> getMonths() {
        return months;
    }

    public void addMonth(Month month) {
        months.add(month);
    }

    public void setMonths(List<Month> months) {
        this.months = months;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMonthId() {
        return monthId;
    }

    public void setMonthId(int monthId) {
        this.monthId = monthId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Weather weather = (Weather) o;

        if (monthId != weather.monthId) return false;
        return months != null ? months.equals(weather.months) : weather.months == null;
    }

    @Override
    public int hashCode() {
        int result = monthId;
        result = 31 * result + (months != null ? months.hashCode() : 0);
        return result;
    }
}
