package com.example.nazarii_moshenskyi.countryinfo.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "currency")
public class Currency {
    @PrimaryKey(autoGenerate = true)
    private long id;

    @Ignore
    private String currencyName;
    private String code;
    private String symbol;
    private String rate;
    @Ignore
    private List<Compare> compare = null;

    public Currency() {
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public List<Compare> getCompare() {
        return compare;
    }

    public void setCompare(List<Compare> compare) {
        this.compare = compare;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Currency currency = (Currency) o;

        if (currencyName != null ? !currencyName.equals(currency.currencyName) : currency.currencyName != null)
            return false;
        if (code != null ? !code.equals(currency.code) : currency.code != null) return false;
        if (symbol != null ? !symbol.equals(currency.symbol) : currency.symbol != null)
            return false;
        if (rate != null ? !rate.equals(currency.rate) : currency.rate != null) return false;
        return compare != null ? compare.equals(currency.compare) : currency.compare == null;
    }

    @Override
    public int hashCode() {
        int result = currencyName != null ? currencyName.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (symbol != null ? symbol.hashCode() : 0);
        result = 31 * result + (rate != null ? rate.hashCode() : 0);
        result = 31 * result + (compare != null ? compare.hashCode() : 0);
        return result;
    }
}
