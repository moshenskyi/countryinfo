package com.example.nazarii_moshenskyi.countryinfo.interactor.repository;

import com.example.nazarii_moshenskyi.countryinfo.data.model.Country;
import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryAnalytics;
import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryInfo;
import com.example.nazarii_moshenskyi.countryinfo.interactor.api.CountryAnalyticsService;
import com.example.nazarii_moshenskyi.countryinfo.interactor.api.CountryInfoService;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class WebServiceImpl implements WebService {
    private static final String TAG = "WebServiceImpl";
    private CountryInfoService countryInfoService;
    private CountryAnalyticsService countryAnalyticsService;

    public WebServiceImpl(CountryInfoService countryInfoService,
                          CountryAnalyticsService countryAnalyticsService) {
        this.countryInfoService = countryInfoService;
        this.countryAnalyticsService = countryAnalyticsService;
    }

    @Override
    public Observable<CountryInfo> getInfo(String countryName) {
        return countryInfoService.getInfo(countryName)
                .subscribeOn(Schedulers.io())
                .onErrorResumeNext(throwable -> {
                    return Observable.create(emitter -> {
                        emitter.onNext(new CountryInfo());
                        emitter.onComplete();
                    });
                });
    }

    public Observable<List<CountryAnalytics>> getAnalytics(String countryName) {
        return countryAnalyticsService.getAnalytics(countryName)
                .subscribeOn(Schedulers.io())
                .onErrorResumeNext(throwable -> {
                    return Observable.create(emitter -> {
                        emitter.onNext(Collections.emptyList());
                        emitter.onComplete();
                    });
                });
    }

    @Override
    public Observable<List<Country>> getCountries() {
        return countryInfoService.getCountries()
                .subscribeOn(Schedulers.io());
    }

}
