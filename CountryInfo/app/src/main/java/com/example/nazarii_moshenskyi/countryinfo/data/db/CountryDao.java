package com.example.nazarii_moshenskyi.countryinfo.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.nazarii_moshenskyi.countryinfo.data.model.Country;

import java.util.List;

@Dao
public interface CountryDao {

    @Query("SELECT * FROM countries")
    List<Country> getCountries();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCountry(Country country);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCountries(List<Country> country);

}
