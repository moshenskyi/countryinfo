package com.example.nazarii_moshenskyi.countryinfo.dependecies;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.nazarii_moshenskyi.countryinfo.BuildConfig;
import com.example.nazarii_moshenskyi.countryinfo.data.db.AppDataBase;
import com.example.nazarii_moshenskyi.countryinfo.interactor.api.CountryAnalyticsService;
import com.example.nazarii_moshenskyi.countryinfo.interactor.api.CountryInfoService;
import com.example.nazarii_moshenskyi.countryinfo.interactor.repository.DataManager;
import com.example.nazarii_moshenskyi.countryinfo.interactor.repository.WebService;
import com.example.nazarii_moshenskyi.countryinfo.interactor.repository.WebServiceImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    private Context context;

    public DataModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    DataManager provideDataManager(AppDataBase dataBase, WebService webService) {
        return new DataManager(dataBase, webService);
    }

    @Singleton
    @Provides
    WebService provideWebService(CountryInfoService countryInfoService,
                                 CountryAnalyticsService countryAnalyticsService) {

        return new WebServiceImpl(countryInfoService, countryAnalyticsService);
    }

    @Provides
    @Singleton
    AppDataBase provideDatabase() {
        return Room.databaseBuilder(context.getApplicationContext(), AppDataBase.class, BuildConfig.DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

}
