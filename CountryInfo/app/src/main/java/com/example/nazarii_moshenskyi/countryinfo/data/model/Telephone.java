package com.example.nazarii_moshenskyi.countryinfo.data.model;

import com.google.gson.annotations.SerializedName;

public class Telephone {

    @SerializedName("calling_code")
    private String callingCode;
    private String police;
    private String ambulance;
    private String fire;

    public Telephone() {
    }

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    public String getPolice() {
        return police;
    }

    public void setPolice(String police) {
        this.police = police;
    }

    public String getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(String ambulance) {
        this.ambulance = ambulance;
    }

    public String getFire() {
        return fire;
    }

    public void setFire(String fire) {
        this.fire = fire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Telephone telephone = (Telephone) o;

        if (callingCode != null ? !callingCode.equals(telephone.callingCode) : telephone.callingCode != null)
            return false;
        if (police != null ? !police.equals(telephone.police) : telephone.police != null)
            return false;
        if (ambulance != null ? !ambulance.equals(telephone.ambulance) : telephone.ambulance != null)
            return false;
        return fire != null ? fire.equals(telephone.fire) : telephone.fire == null;
    }

    @Override
    public int hashCode() {
        int result = callingCode != null ? callingCode.hashCode() : 0;
        result = 31 * result + (police != null ? police.hashCode() : 0);
        result = 31 * result + (ambulance != null ? ambulance.hashCode() : 0);
        result = 31 * result + (fire != null ? fire.hashCode() : 0);
        return result;
    }
}
