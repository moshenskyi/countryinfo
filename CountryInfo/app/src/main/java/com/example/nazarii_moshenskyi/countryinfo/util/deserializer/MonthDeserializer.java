package com.example.nazarii_moshenskyi.countryinfo.util.deserializer;

import com.example.nazarii_moshenskyi.countryinfo.data.model.Month;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import static com.example.nazarii_moshenskyi.countryinfo.data.model.Month.MonthContract.PAVG;
import static com.example.nazarii_moshenskyi.countryinfo.data.model.Month.MonthContract.PMAX;
import static com.example.nazarii_moshenskyi.countryinfo.data.model.Month.MonthContract.PMIN;
import static com.example.nazarii_moshenskyi.countryinfo.data.model.Month.MonthContract.TAVG;
import static com.example.nazarii_moshenskyi.countryinfo.data.model.Month.MonthContract.TMAX;
import static com.example.nazarii_moshenskyi.countryinfo.data.model.Month.MonthContract.TMIN;

public class MonthDeserializer implements JsonDeserializer<Month> {
    @Override
    public Month deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Month month = new Month();
        JsonObject monthObject = json.getAsJsonObject();

        month.setTMin(monthObject.get(TMIN).getAsString());
        month.setTMax(monthObject.get(TMAX).getAsString());
        month.setTAvg(monthObject.get(TAVG).getAsString());
        month.setPMin(monthObject.get(PMIN).getAsString());
        month.setPMax(monthObject.get(PMAX).getAsString());
        month.setPAvg(monthObject.get(PAVG).getAsString());

        return month;
    }
}
