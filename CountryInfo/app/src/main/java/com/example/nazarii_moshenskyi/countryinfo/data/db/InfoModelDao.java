package com.example.nazarii_moshenskyi.countryinfo.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import com.example.nazarii_moshenskyi.countryinfo.data.model.Country;
import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryInfo;
import com.example.nazarii_moshenskyi.countryinfo.data.model.InfoModel;

import java.util.List;

@Dao
public interface InfoModelDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCountries(List<Country> info);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertInfoModel(InfoModel model);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCountryInfo(CountryInfo info);

}
