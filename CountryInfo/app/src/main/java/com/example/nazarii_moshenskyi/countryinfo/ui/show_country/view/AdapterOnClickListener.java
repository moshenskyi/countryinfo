package com.example.nazarii_moshenskyi.countryinfo.ui.show_country.view;

import com.example.nazarii_moshenskyi.countryinfo.data.model.Country;

public interface AdapterOnClickListener {

    void onClick(Country country);

}
