package com.example.nazarii_moshenskyi.countryinfo.ui.base;

public interface BaseRxMvpView extends BaseMvpView {

    void showLoadingBar();

    void hideLoadingBar();

}
