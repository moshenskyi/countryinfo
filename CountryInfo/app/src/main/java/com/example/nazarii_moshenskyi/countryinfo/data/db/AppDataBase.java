package com.example.nazarii_moshenskyi.countryinfo.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.nazarii_moshenskyi.countryinfo.data.model.Country;
import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryAnalytics;
import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryInfo;
import com.example.nazarii_moshenskyi.countryinfo.data.model.Currency;
import com.example.nazarii_moshenskyi.countryinfo.data.model.InfoModel;
import com.example.nazarii_moshenskyi.countryinfo.data.model.Month;
import com.example.nazarii_moshenskyi.countryinfo.data.model.Names;
import com.example.nazarii_moshenskyi.countryinfo.data.model.Weather;

@Database(entities = { CountryAnalytics.class, CountryInfo.class, Currency.class,
        InfoModel.class, Names.class, Country.class, Month.class, Weather.class },
        version = 2, exportSchema = false)
public abstract class AppDataBase extends RoomDatabase {

    public abstract InfoModelDao infoModelDao();

    public abstract CountryDao countryDao();

    public abstract AnalyticsDao analyticsDao();

    public abstract CountryInfoDao countryInfoDao();

    public abstract CurrencyDao currencyDao();

    public abstract MonthDao monthDao();

    public abstract WeatherDao weatherDao();

}
