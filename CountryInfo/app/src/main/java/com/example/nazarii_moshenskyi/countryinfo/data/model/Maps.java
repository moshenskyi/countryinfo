package com.example.nazarii_moshenskyi.countryinfo.data.model;

import com.google.gson.annotations.SerializedName;

public class Maps {

    private String lat;
    @SerializedName("long")
    private String _long;
    private String zoom;

    public Maps() {
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }

    public String getZoom() {
        return zoom;
    }

    public void setZoom(String zoom) {
        this.zoom = zoom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Maps maps = (Maps) o;

        if (lat != null ? !lat.equals(maps.lat) : maps.lat != null) return false;
        if (_long != null ? !_long.equals(maps._long) : maps._long != null) return false;
        return zoom != null ? zoom.equals(maps.zoom) : maps.zoom == null;
    }

    @Override
    public int hashCode() {
        int result = lat != null ? lat.hashCode() : 0;
        result = 31 * result + (_long != null ? _long.hashCode() : 0);
        result = 31 * result + (zoom != null ? zoom.hashCode() : 0);
        return result;
    }
}
