package com.example.nazarii_moshenskyi.countryinfo.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "weather_by_month",
        foreignKeys = {
        @ForeignKey(entity = Month.class, parentColumns = "id", childColumns = "month_id"),
        @ForeignKey(entity = Weather.class, parentColumns = "id", childColumns = "weather_id")})
public class WeatherByMonth {
    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "weather_id")
    private long weatherId;

    @ColumnInfo(name = "month_id")
    private int monthId;

    public long getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(long weatherId) {
        this.weatherId = weatherId;
    }

    public int getMonthId() {
        return monthId;
    }

    public void setMonthId(int monthId) {
        this.monthId = monthId;
    }

}
