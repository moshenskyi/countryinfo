package com.example.nazarii_moshenskyi.countryinfo.ui.show_country.presenter;

import com.example.nazarii_moshenskyi.countryinfo.ui.base.BaseMvpPresenter;
import com.example.nazarii_moshenskyi.countryinfo.ui.show_country.view.CountryMvpView;
import com.example.nazarii_moshenskyi.countryinfo.ui.show_country.view.recycler.CountryAdapter;

public interface CountryMvpPresenter extends BaseMvpPresenter<CountryMvpView> {

    void getCountries();

    void updateItems(String input, CountryAdapter adapter);

}
