package com.example.nazarii_moshenskyi.countryinfo.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Month {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    private String tMin;

    private String tMax;

    private String tAvg;

    private String pMin;

    private String pMax;

    private String pAvg;

    public Month() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTMin() {
        return tMin;
    }

    public void setTMin(String tMin) {
        this.tMin = tMin;
    }

    public String getTMax() {
        return tMax;
    }

    public void setTMax(String tMax) {
        this.tMax = tMax;
    }

    public String getTAvg() {
        return tAvg;
    }

    public void setTAvg(String tAvg) {
        this.tAvg = tAvg;
    }

    public String getPMin() {
        return pMin;
    }

    public void setPMin(String pMin) {
        this.pMin = pMin;
    }

    public String getPMax() {
        return pMax;
    }

    public void setPMax(String pMax) {
        this.pMax = pMax;
    }

    public String getPAvg() {
        return pAvg;
    }

    public void setPAvg(String pAvg) {
        this.pAvg = pAvg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public interface MonthContract {

        public String TMIN = "tMin";

        public String TMAX = "tMax";

        public String TAVG = "tAvg";

        public String PMIN = "pMin";

        public String PMAX = "pMax";

        public String PAVG = "pAvg";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Month month = (Month) o;

        if (name != null ? !name.equals(month.name) : month.name != null) return false;
        if (tMin != null ? !tMin.equals(month.tMin) : month.tMin != null) return false;
        if (tMax != null ? !tMax.equals(month.tMax) : month.tMax != null) return false;
        if (tAvg != null ? !tAvg.equals(month.tAvg) : month.tAvg != null) return false;
        if (pMin != null ? !pMin.equals(month.pMin) : month.pMin != null) return false;
        if (pMax != null ? !pMax.equals(month.pMax) : month.pMax != null) return false;
        return pAvg != null ? pAvg.equals(month.pAvg) : month.pAvg == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (tMin != null ? tMin.hashCode() : 0);
        result = 31 * result + (tMax != null ? tMax.hashCode() : 0);
        result = 31 * result + (tAvg != null ? tAvg.hashCode() : 0);
        result = 31 * result + (pMin != null ? pMin.hashCode() : 0);
        result = 31 * result + (pMax != null ? pMax.hashCode() : 0);
        result = 31 * result + (pAvg != null ? pAvg.hashCode() : 0);
        return result;
    }
}
