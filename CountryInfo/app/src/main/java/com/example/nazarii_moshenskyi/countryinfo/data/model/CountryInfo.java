package com.example.nazarii_moshenskyi.countryinfo.data.model;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "country_info",
        foreignKeys = {@ForeignKey(entity = Currency.class,
                                    parentColumns = "id",
                                    childColumns = "currency_id", onDelete = ForeignKey.CASCADE),
                       @ForeignKey(entity = Names.class,
                                    parentColumns = "id",
                                    childColumns = "names_id", onDelete = ForeignKey.CASCADE)})
public class CountryInfo {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @Ignore
    private String name;

    @ColumnInfo(name = "currency_id")
    private long currencyId;

    @ColumnInfo(name = "names_id")
    private long namesId;

    @Ignore
    private Names names;

    @Embedded
    private Maps maps;

    @Embedded
    private Timezone timezone;

    @Ignore
    private List<Language> language = null;

    @Embedded
    private Electricity electricity;

    @Embedded
    private Telephone telephone;

    @Embedded
    private Water water;

    @Ignore
    private List<Vaccine> vaccinations = null;

    @Ignore
    private Currency currency;

    @Ignore
    private Weather weather;

    @Embedded
    private Advise advise;

    @Ignore
    private List<Neighbor> neighbors;

    public CountryInfo() {
    }

    public Names getNames() {
        return names;
    }

    public void setNames(Names names) {
        this.names = names;
    }

    public Maps getMaps() {
        return maps;
    }

    public void setMaps(Maps maps) {
        this.maps = maps;
    }

    public Timezone getTimezone() {
        return timezone;
    }

    public void setTimezone(Timezone timezone) {
        this.timezone = timezone;
    }

    public List<Language> getLanguage() {
        return language;
    }

    public void setLanguage(List<Language> language) {
        this.language = language;
    }

    public Electricity getElectricity() {
        return electricity;
    }

    public void setElectricity(Electricity electricity) {
        this.electricity = electricity;
    }

    public Telephone getTelephone() {
        return telephone;
    }

    public void setTelephone(Telephone telephone) {
        this.telephone = telephone;
    }

    public Water getWater() {
        return water;
    }

    public void setWater(Water water) {
        this.water = water;
    }

    public List<Vaccine> getVaccinations() {
        return vaccinations;
    }

    public void setVaccinations(List<Vaccine> vaccinations) {
        this.vaccinations = vaccinations;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Advise getAdvise() {
        return advise;
    }

    public void setAdvise(Advise advise) {
        this.advise = advise;
    }

    public List<Neighbor> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(List<Neighbor> neighbors) {
        this.neighbors = neighbors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    public long getNamesId() {
        return namesId;
    }

    public void setNamesId(long namesId) {
        this.namesId = namesId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountryInfo that = (CountryInfo) o;

        if (currencyId != that.currencyId) return false;
        if (namesId != that.namesId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (names != null ? !names.equals(that.names) : that.names != null) return false;
        if (maps != null ? !maps.equals(that.maps) : that.maps != null) return false;
        if (timezone != null ? !timezone.equals(that.timezone) : that.timezone != null)
            return false;
        if (language != null ? !language.equals(that.language) : that.language != null)
            return false;
        if (electricity != null ? !electricity.equals(that.electricity) : that.electricity != null)
            return false;
        if (telephone != null ? !telephone.equals(that.telephone) : that.telephone != null)
            return false;
        if (water != null ? !water.equals(that.water) : that.water != null) return false;
        if (vaccinations != null ? !vaccinations.equals(that.vaccinations) : that.vaccinations != null)
            return false;
        if (currency != null ? !currency.equals(that.currency) : that.currency != null)
            return false;
        if (weather != null ? !weather.equals(that.weather) : that.weather != null) return false;
        if (advise != null ? !advise.equals(that.advise) : that.advise != null) return false;
        return neighbors != null ? neighbors.equals(that.neighbors) : that.neighbors == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (int) (currencyId ^ (currencyId >>> 32));
        result = 31 * result + (int) (namesId ^ (namesId >>> 32));
        result = 31 * result + (names != null ? names.hashCode() : 0);
        result = 31 * result + (maps != null ? maps.hashCode() : 0);
        result = 31 * result + (timezone != null ? timezone.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (electricity != null ? electricity.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (water != null ? water.hashCode() : 0);
        result = 31 * result + (vaccinations != null ? vaccinations.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (weather != null ? weather.hashCode() : 0);
        result = 31 * result + (advise != null ? advise.hashCode() : 0);
        result = 31 * result + (neighbors != null ? neighbors.hashCode() : 0);
        return result;
    }
}
