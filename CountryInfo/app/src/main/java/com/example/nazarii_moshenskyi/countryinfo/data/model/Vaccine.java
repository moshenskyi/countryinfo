package com.example.nazarii_moshenskyi.countryinfo.data.model;

//@Entity
public class Vaccine {
    private String name;
    private String message;

    public Vaccine() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vaccine vaccine = (Vaccine) o;

        if (name != null ? !name.equals(vaccine.name) : vaccine.name != null) return false;
        return message != null ? message.equals(vaccine.message) : vaccine.message == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }
}
