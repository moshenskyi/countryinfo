package com.example.nazarii_moshenskyi.countryinfo.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryAnalytics;

@Dao
public interface AnalyticsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAnalytics(CountryAnalytics analytics);
}
