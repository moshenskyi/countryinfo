package com.example.nazarii_moshenskyi.countryinfo.data.model;

public class Timezone {

    private String timezoneName;

    public Timezone() {
    }

    public String getTimezoneName() {
        return timezoneName;
    }

    public void setTimezoneName(String timezoneName) {
        this.timezoneName = timezoneName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Timezone timezone = (Timezone) o;

        return timezoneName != null ? timezoneName.equals(timezone.timezoneName) : timezone.timezoneName == null;
    }

    @Override
    public int hashCode() {
        return timezoneName != null ? timezoneName.hashCode() : 0;
    }
}
