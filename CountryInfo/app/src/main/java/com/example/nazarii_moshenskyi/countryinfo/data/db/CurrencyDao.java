package com.example.nazarii_moshenskyi.countryinfo.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import com.example.nazarii_moshenskyi.countryinfo.data.model.Currency;

@Dao
public interface CurrencyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertCurrency(Currency currency);
}
