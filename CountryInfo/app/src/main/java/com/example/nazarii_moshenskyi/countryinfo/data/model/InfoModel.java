package com.example.nazarii_moshenskyi.countryinfo.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;


@Entity(tableName = "model",
        foreignKeys = { @ForeignKey(entity = CountryInfo.class,
                                    parentColumns = "id",
                                    childColumns = "country_info_id", onDelete = ForeignKey.CASCADE),
                        @ForeignKey(entity = CountryAnalytics.class,
                                    parentColumns = "id",
                                    childColumns = "analytics_id", onDelete = ForeignKey.CASCADE)
        })
public class InfoModel {
    private static final String TAG = "InfoModel";

    @PrimaryKey(autoGenerate = true)
    private long id;

    private String name;

    @ColumnInfo(name = "analytics_id")
    private long analyticsId;

    @ColumnInfo(name = "country_info_id")
    private long countryInfoId;

    @Ignore
    private CountryInfo countryInfo;

    @Ignore
    private List<CountryAnalytics> analytics;

    public InfoModel() {
    }

    public List<CountryAnalytics> getAnalytics() {
        return analytics;
    }

    public void setAnalytics(List<CountryAnalytics> analytics) {
        Log.d(TAG, "setAnalytics: is null =  " + (analytics == null));
        this.analytics = analytics;
    }

    public CountryInfo getCountryInfo() {
        return countryInfo;
    }

    public void setCountryInfo(CountryInfo countryInfo) {
        this.countryInfo = countryInfo;
    }

    public long getAnalyticsId() {
        return analyticsId;
    }

    public void setAnalyticsId(long analyticsId) {
        this.analyticsId = analyticsId;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCountryInfoId() {
        return countryInfoId;
    }

    public void setCountryInfoId(long countryInfoId) {
        this.countryInfoId = countryInfoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InfoModel infoModel = (InfoModel) o;

        if (analyticsId != infoModel.analyticsId) return false;
        if (countryInfoId != infoModel.countryInfoId) return false;
        if (name != null ? !name.equals(infoModel.name) : infoModel.name != null) return false;
        if (countryInfo != null ? !countryInfo.equals(infoModel.countryInfo) : infoModel.countryInfo != null)
            return false;
        return analytics != null ? analytics.equals(infoModel.analytics) : infoModel.analytics == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (int) (analyticsId ^ (analyticsId >>> 32));
        result = 31 * result + (int) (countryInfoId ^ (countryInfoId >>> 32));
        result = 31 * result + (countryInfo != null ? countryInfo.hashCode() : 0);
        result = 31 * result + (analytics != null ? analytics.hashCode() : 0);
        return result;
    }
}
