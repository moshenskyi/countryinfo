package com.example.nazarii_moshenskyi.countryinfo.interactor.repository;

import android.util.Log;

import com.example.nazarii_moshenskyi.countryinfo.data.db.AppDataBase;
import com.example.nazarii_moshenskyi.countryinfo.data.model.Country;
import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryAnalytics;
import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryInfo;
import com.example.nazarii_moshenskyi.countryinfo.data.model.InfoModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class DataManager {
    private static final String TAG = "DataManager";
    private AppDataBase dataBase;
    private WebService webService;

    public DataManager(AppDataBase dataBase, WebService webService) {
        this.webService = webService;
        this.dataBase = dataBase;
    }

    public Observable<InfoModel> getInfo(final String countryName) {
        final InfoModel infoModel = new InfoModel();

        Observable<CountryInfo> info = webService.getInfo(countryName);
        Observable<List<CountryAnalytics>> analytics = webService.getAnalytics(countryName);
        Observable<InfoModel> zippedObservable = Observable.zip(info, analytics,
                (countryInfo, analyticsObservable) -> {
                    Log.d(TAG, "apply:" + countryName + " == null is " + (countryInfo == null));
                    infoModel.setAnalytics(analyticsObservable);
                    infoModel.setCountryInfo(countryInfo);
                    return infoModel;
                });

        analytics
                .subscribeOn(Schedulers.io())
                .flatMapIterable(list -> list)
                .subscribe(analyticsItem -> dataBase.analyticsDao().insertAnalytics(analyticsItem));

        zippedObservable.subscribeOn(Schedulers.io())
                .subscribe(model -> {
                            CountryInfo countryInfo = model.getCountryInfo();
                            countryInfo.setCurrencyId(dataBase.currencyDao().insertCurrency(countryInfo.getCurrency()));
                            long namesId = dataBase.countryInfoDao().insertNames(countryInfo.getNames());
                            countryInfo.setNamesId(namesId);
                            //dataBase.monthDao().insertMonths(countryInfo.getWeather().getMonths());
                            //dataBase.countryInfoDao().insertWeather(countryInfo.getWeather());
                            model.setCountryInfoId(dataBase.countryInfoDao().insertCountryInfo(countryInfo));
                            model.setAnalyticsId(namesId);
                            dataBase.infoModelDao().insertInfoModel(model);

                        }, error -> Log.d(TAG, "getInfo: " + error.getMessage()));

        return zippedObservable;
    }

    public Observable<List<Country>> getCountries() {
        Observable<List<Country>> countries = webService.getCountries();

        countries
                .subscribeOn(Schedulers.io())
                .flatMapIterable(countryList -> countryList)
                .subscribe(country -> dataBase.countryDao().insertCountry(country));
        return countries;
    }
}
