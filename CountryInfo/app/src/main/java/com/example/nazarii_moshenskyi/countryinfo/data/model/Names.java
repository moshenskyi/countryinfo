package com.example.nazarii_moshenskyi.countryinfo.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "names")
public class Names {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String name;
    private String full;
    private String iso2;
    private String iso3;
    private String continent;

    public Names() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }

    public String getIso3() {
        return iso3;
    }

    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Names names = (Names) o;

        if (name != null ? !name.equals(names.name) : names.name != null) return false;
        if (full != null ? !full.equals(names.full) : names.full != null) return false;
        if (iso2 != null ? !iso2.equals(names.iso2) : names.iso2 != null) return false;
        if (iso3 != null ? !iso3.equals(names.iso3) : names.iso3 != null) return false;
        return continent != null ? continent.equals(names.continent) : names.continent == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (full != null ? full.hashCode() : 0);
        result = 31 * result + (iso2 != null ? iso2.hashCode() : 0);
        result = 31 * result + (iso3 != null ? iso3.hashCode() : 0);
        result = 31 * result + (continent != null ? continent.hashCode() : 0);
        return result;
    }
}
