package com.example.nazarii_moshenskyi.countryinfo.data.model;

public class Language {

    private String language;
    private String official;

    public Language() {
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getOfficial() {
        return official;
    }

    public void setOfficial(String official) {
        this.official = official;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Language language1 = (Language) o;

        if (language != null ? !language.equals(language1.language) : language1.language != null)
            return false;
        return official != null ? official.equals(language1.official) : language1.official == null;
    }

    @Override
    public int hashCode() {
        int result = language != null ? language.hashCode() : 0;
        result = 31 * result + (official != null ? official.hashCode() : 0);
        return result;
    }
}
