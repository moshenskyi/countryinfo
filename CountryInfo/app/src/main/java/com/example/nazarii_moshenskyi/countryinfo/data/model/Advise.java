package com.example.nazarii_moshenskyi.countryinfo.data.model;

public class Advise {

    public Advise() {
        advise = "";
    }

    private String advise;

    public String getAdvise() {
        return advise;
    }

    public void setAdvise(String advise) {
        this.advise = advise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Advise advise1 = (Advise) o;

        return advise != null ? advise.equals(advise1.advise) : advise1.advise == null;
    }

    @Override
    public int hashCode() {
        return advise != null ? advise.hashCode() : 0;
    }
}
