package com.example.nazarii_moshenskyi.countryinfo.data.model;


import android.arch.persistence.room.Ignore;

import java.util.List;

public class Electricity {
    private String voltage;
    private String frequency;

    @Ignore //////////////////
    private List<String> plugs = null;

    @Ignore
    private String data;

    public Electricity() {
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public List<String> getPlugs() {
        return plugs;
    }

    public void setPlugs(List<String> plugs) {
        this.plugs = plugs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Electricity that = (Electricity) o;

        if (voltage != null ? !voltage.equals(that.voltage) : that.voltage != null) return false;
        if (frequency != null ? !frequency.equals(that.frequency) : that.frequency != null)
            return false;
        if (plugs != null ? !plugs.equals(that.plugs) : that.plugs != null) return false;
        return data != null ? data.equals(that.data) : that.data == null;
    }

    @Override
    public int hashCode() {
        int result = voltage != null ? voltage.hashCode() : 0;
        result = 31 * result + (frequency != null ? frequency.hashCode() : 0);
        result = 31 * result + (plugs != null ? plugs.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }
}
