package com.example.nazarii_moshenskyi.countryinfo.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "analytics")
public class CountryAnalytics {

    @PrimaryKey(autoGenerate = true)
    private long id;

    private String flag;
    private Integer population;
    private Integer area;

    @Ignore /////////////////////
    private List<String> timezones;

    public CountryAnalytics() {
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public List<String> getTimezones() {
        return timezones;
    }

    public void setTimezones(List<String> timezones) {
        this.timezones = timezones;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountryAnalytics that = (CountryAnalytics) o;

        if (flag != null ? !flag.equals(that.flag) : that.flag != null) return false;
        if (population != null ? !population.equals(that.population) : that.population != null)
            return false;
        if (area != null ? !area.equals(that.area) : that.area != null) return false;
        return timezones != null ? timezones.equals(that.timezones) : that.timezones == null;
    }

    @Override
    public int hashCode() {
        int result = flag != null ? flag.hashCode() : 0;
        result = 31 * result + (population != null ? population.hashCode() : 0);
        result = 31 * result + (area != null ? area.hashCode() : 0);
        result = 31 * result + (timezones != null ? timezones.hashCode() : 0);
        return result;
    }
}
