package com.example.nazarii_moshenskyi.countryinfo.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import com.example.nazarii_moshenskyi.countryinfo.data.model.Month;

@Dao
public interface MonthDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertMonth(Month month);
}
