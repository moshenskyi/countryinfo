package com.example.nazarii_moshenskyi.countryinfo.interactor.repository;

import com.example.nazarii_moshenskyi.countryinfo.data.model.Country;
import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryAnalytics;
import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryInfo;

import java.util.List;

import io.reactivex.Observable;

public interface WebService {

    Observable<CountryInfo> getInfo(String countryName);

    Observable<List<Country>> getCountries();

    Observable<List<CountryAnalytics>> getAnalytics(String countryName);

}
