package com.example.nazarii_moshenskyi.countryinfo.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import com.example.nazarii_moshenskyi.countryinfo.data.model.CountryInfo;
import com.example.nazarii_moshenskyi.countryinfo.data.model.Names;

@Dao
public interface CountryInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertCountryInfo(CountryInfo countryInfo);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertNames(Names names);

    /*@Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertWeather(Weather weather);*/

}
