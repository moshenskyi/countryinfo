package com.example.nazarii_moshenskyi.countryinfo.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import com.example.nazarii_moshenskyi.countryinfo.data.model.Weather;

@Dao
public interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertWeather(Weather weather);

}
