package com.example.nazarii_moshenskyi.countryinfo.data.model;

import com.google.gson.annotations.SerializedName;

public class Water {

    @SerializedName("short")
    private String shortInfo;
    @SerializedName("full")
    private String fullInfo;

    public Water() {
    }

    public String getShortInfo() {
        return shortInfo;
    }

    public void setShortInfo(String shortInfo) {
        this.shortInfo = shortInfo;
    }

    public String getFullInfo() {
        return fullInfo;
    }

    public void setFullInfo(String fullInfo) {
        this.fullInfo = fullInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Water water = (Water) o;

        if (shortInfo != null ? !shortInfo.equals(water.shortInfo) : water.shortInfo != null)
            return false;
        return fullInfo != null ? fullInfo.equals(water.fullInfo) : water.fullInfo == null;
    }

    @Override
    public int hashCode() {
        int result = shortInfo != null ? shortInfo.hashCode() : 0;
        result = 31 * result + (fullInfo != null ? fullInfo.hashCode() : 0);
        return result;
    }
}
