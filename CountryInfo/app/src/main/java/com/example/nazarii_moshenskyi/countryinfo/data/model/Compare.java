package com.example.nazarii_moshenskyi.countryinfo.data.model;

public class Compare {

    public Compare() {
    }

    private String name;

    private String rate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Compare compare = (Compare) o;

        if (name != null ? !name.equals(compare.name) : compare.name != null) return false;
        return rate != null ? rate.equals(compare.rate) : compare.rate == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (rate != null ? rate.hashCode() : 0);
        return result;
    }
}
